package com.crud.dao;

import com.crud.entity.Employee;

import java.util.List;

public interface EmployeeDao {
    List<Employee> findAll();

    void insertEmployee(Employee emp);

    void updateEmployee(Employee emp);

    void executeUpdateEmployee(Employee emp);

    void deleteEmployee(Employee emp);
}
