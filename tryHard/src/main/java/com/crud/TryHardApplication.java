package com.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages="com.crud")
public class TryHardApplication {

	public static void main(String[] args) {
		SpringApplication.run(TryHardApplication.class, args);
	}

}
